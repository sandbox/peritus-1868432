<?php
/**
 * @file
 * Helper function for Bibliography ISBN module.
 */

// Define some ISBNDB constants
define('BIBLIO_ISBN_ISBNDB_URL', 'http://isbndb.com/api/books.xml');
define('BIBLIO_ISBN_OPENLIB_URL', 'http://openlibrary.org/api/books');


/*
 * Fix ISBN to be digits only.
 *
 * @param string $isbn
 *   ISBN of book.
 *
 * @return int
 *   The sanitized ISBN.
 */
function _biblio_isbn_normalize($isbn) {
  return preg_replace('/[^0-9]/', '', $isbn);
}

/*
 * Check number of found items in XML data.
 *
 * @param string $data
 *   XML data from data source.
 *
 * @return int
 *   Number of found items.
 */
function _biblio_isbn_found($data) {
  switch (BIBLIO_ISBN_SOURCE) {
    case 'isbndb':
      return isset($data['1']['attributes']['TOTAL_RESULTS'])
        ? (int) $data['1']['attributes']['TOTAL_RESULTS']
        : 0;

    case 'openlib':
      return count($data);

    default:
      return 0;
  }
}

/*
 * Get book data from external database.
 *
 * @param string $isbn
 *   ISBN of book.
 *
 * @return object
 *   Book information fetched from data source.
 */
function _biblio_isbn_fetch($isbn) {
  switch (BIBLIO_ISBN_SOURCE) {

    // ISBNdb
    case 'isbndb':
      // Get information from ISNBdb
      $url = url(BIBLIO_ISBN_ISBNDB_URL, array('query' => array(
        'access_key' => BIBLIO_ISBN_ISBNDB_ACCESS_KEY,
        'index1' => 'isbn',
        'results' => 'texts,authors',
        'value1' => $isbn
      )));
      $response = drupal_http_request($url);

      // Log http if fails
      if (!$response) {
        watchdog('biblio_isbn', 'Failed sending request to ISBNdb information service.', array(), WATCHDOG_ERROR);
        return FALSE;
      }

      // Build an array from response data
      $xml_parser = drupal_xml_parser_create($response->data);
      if (!xml_parse_into_struct($xml_parser, $response->data, $data)) {
        watchdog('biblio_isbn', 'Error building array from XML returned from ISBNdb information service.', array(), WATCHDOG_ERROR);
        return FALSE;
      }
      xml_parser_free($xml_parser); // FIXME: free on error?

      // Return an array if everything is success
      return $data;

    // Open Library
    case 'openlib':
      // Get information from ISNBdb
      $url = url(BIBLIO_ISBN_OPENLIB_URL, array('query' => array(
        'bibkeys' => 'ISBN:' . $isbn,
        'jscmd' => 'data',
        'format' => 'json',
      )));
      $response = drupal_http_request($url);

      // Log http if fails
      if (!$response) {
        watchdog('biblio_isbn', 'Failed sending request to Open Library information service.', array(), WATCHDOG_ERROR);
        return FALSE;
      }

      $data = json_decode($response->data);
      if (!$data) {
        watchdog('biblio_isbn', 'Error building array from JSON returned from Open Library information service.', array(), WATCHDOG_ERROR);
        return FALSE;
      }

      // Return an array if everything is success
      return $data;

    default:
      return FALSE;
  }

}

/*
 * Parse values from given data.
 *
 * @param array $data
 *   Data fetched from source.
 * @param string $isbn
 *   ISBN of book.
 *
 * @return array
 *   Book information parsed from data given from source.
 */
function _biblio_isbn_parse($data, $isbn) {
  switch (BIBLIO_ISBN_SOURCE) {

    // ISBNdb
    case 'isbndb':
      // Needles
      $information = array(
        'title' => '',
        'titlelong' => '',
        'authorstext' => '',
        'publishertext' => '',
        'summary' => '',
        'notes' => '',
        // Authors
      );

      // Go through data array to get needles
      foreach ($data as $item) {
        $tag = strtolower($item['tag']);

        // Simple <tag>value</tag> information parsing
        if (array_key_exists($tag, $information)) {
          $information[$tag] = check_plain($item['value']);
        }

        // Additional data parsing for authors
        if ($tag == 'person') {
          if (!isset($information['authors'])) {
            $information['authors'] = array();
          }
          $information['authors'][] = check_plain($item['value']);
        }
      }

      return $information;

    // Open Library
    case 'openlib':
      if (!isset($data->{'ISBN:' . $isbn})) {
        return FALSE;
      }
      return (array) $data->{'ISBN:' . $isbn};

    default:
      return FALSE;
  }


}
